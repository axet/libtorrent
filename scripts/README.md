# compile for android-torrent-client

    # go to .../android-torrent-client
    # git clone https://gitlab.com/axet/libtorrent
    # cd libtorrent

if you have global env GOPATH variable, run ./local.sh

Optional: preparing gomobile API9 support:

    # source ./scripts/gomobile.sh mod

Optional: installing default 'gomobile':

    # go install golang.org/x/mobile/cmd/gomobile@latest

Download all sources:

    # go get

Make a release:

    # ./scripts/release.sh

Add 'torrent' sources into libtorrent-sources.jar

    # ./scripts/sources.sh mod

Upload to maven:

    # ./scripts/deploy.sh